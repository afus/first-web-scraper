import tkinter as tk
from tkinter import ttk


def print_links(web):
    import bs4 as bs
    import urllib.request

    word = str(key_word.get())

    sauce = urllib.request.urlopen(web).read()

    soup = bs.BeautifulSoup(sauce, 'lxml')

    for url in soup.find_all('a'):
        if word in str(url.string):
            links.insert(tk.END, str(url.get('href')) + '\n')


def check_which_sites_to_search(event):

    new_york_times_web = "https://www.nytimes.com/"
    wall_street_web = "https://www.wsj.com/"

    if new_york_times_state.get():
        print_links(new_york_times_web)
    if wall_street_state.get():
        print_links(wall_street_web)


root = tk.Tk()
# frame = tk.Frame(root)
root.geometry("500x500")

# change later
root.title('Test')


# styling tkk
# style = ttk.Style()
# style.configure("BW.TLabel", foreground="black", background="white")

ttk.Label(root, text="Enter key word").grid(row=0, column=1)
key_word = tk.Entry(root)
key_word.grid(row=1, column='1')

# Design.. Placemets.. comments
new_york_times_state = tk.IntVar(value=1)

new_york_times_check = tk.Checkbutton(
    root, text="New York Times", variable=new_york_times_state, )
new_york_times_check.bind('<Button-1>')
new_york_times_check.grid(row=2, column=1, sticky=tk.W)

wall_street_state = tk.IntVar(value=1)
wall_street_check = tk.Checkbutton(
    root, text="Wall Street Journal", variable=wall_street_state)
wall_street_check.bind('<Button-1>')
wall_street_check.grid(row=2, column=1)

searchButton = ttk.Button(root, text="Search")
searchButton.bind('<Button-1>', check_which_sites_to_search)
searchButton.grid(row=4, column=1)

links = tk.Text(root)
links.grid(row=5, column=1)

root.mainloop()
